%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author: Prakhar Srivastav 
% UNI: PS2894
% Machine Learning - Tony Jebara.
% Date: 30 Sept 2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Driver Program for HW4
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Reading in the data
A = importdata('dataset4.mat');
X = A.X;
Y = A.Y;

% step size
eta = 10; 

% setting a limit on max iterations
iter = 1; maxIter = 40000;

% error tolerance
tolerance = 0.001;

% defining a logistic function
sigmoid = @(x) 1 ./ (1 + exp(-1 .* x));

% initializing theta and newTheta
theta = rand(1, 3); newTheta = [0, 0, 0]; N = length(X);
err = norm(newTheta - theta);
empericalRisks = zeros(1, maxIter);
classificationError = zeros(1, maxIter);

% running the gradient descent
while err > tolerance && iter < maxIter
    objective = Y - (sigmoid(theta*X'))';
    newTheta = theta + ((eta/N) * (X' * objective))';
    err = norm(newTheta - theta);
    
    % get the empirical risk for newTheta
    empericalRisks(iter) = logisticRisk(newTheta, X, Y);
    
    % calculate the classification error
    classificationError(iter) = 100 * sum(round(abs(Y - sigmoid(theta*X')')))/N;
    theta = newTheta;
    iter = iter + 1;
end

% finding the classification error
figure
plot(classificationError);
title('Plot of classification error vs number of iterations');
xlabel('Iteration Count');
ylabel('Classification Error (%)');

% plotting the data and decision boundary
figure
tmp = [X(:, 1) X(:, 2) Y];
tmpOnes = tmp(tmp(:, 3) == 1, :);
tmpZeros = tmp(tmp(:, 3) == 0, :);
x1 = X(:, 1);
getY = @(x) ((-theta(3) - x*theta(1)) / theta(2));
x1s = min(x1): (max(x1) - min(x1)) / 300: max(x1);
x2s = arrayfun(getY, x1s);
plot(tmpOnes(:, 1), tmpOnes(:, 2), '*', tmpZeros(:, 1), tmpZeros(:, 2), 'O', x1s, x2s);
title('Plotting the Decision Boundary on the data');
xlabel('First Feature (x1)');
ylabel('Second Feature (x2)');
legend('Y = 1', 'Y = 0', 'Linear Decision Boundary');

% plotting the emperical risk across varies iterations
figure
plot(empericalRisks(1:iter));
title('Plot of emperical risk vs number of iterations');
xlabel('Iteration Count');
ylabel('Emperical Risk');