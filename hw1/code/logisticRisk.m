%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author: Prakhar Srivastav 
% UNI: PS2894
% Machine Learning - Tony Jebara.
% Date: 30 Sept 2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to calculate logistic risk
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [empericalRisk] = logisticRisk(theta, X, Y)
    sigmoid = @(x) 1 ./ (1 + exp(-1 .* x));
    N = length(X);
    
    loss = (1/N) * ((Y - 1)' .* log(1 - sigmoid(theta*X')) +  ...
                ((-Y)' .* log(sigmoid(theta*X'))));
    loss = loss(~isnan(loss)); 
    empericalRisk = sum(loss)/N;
end