%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author: Prakhar Srivastav 
% UNI: PS2894
% Machine Learning - Tony Jebara.
% Date: 30 Sept 2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Driver Program for HW2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Reading in the data
A = importdata('problem2.mat');
x = A.x; y = A.y; N = length(x);
indices = randperm(N);

% 50-50 split between train and test data
splitBoundary = 0.5 * N;

% create the training dataset
xTrain = x(indices(1:splitBoundary), :);
yTrain = y(indices(1:splitBoundary), :);

% create the test dataset
xTest = x(indices(splitBoundary + 1:length(x)), :);
yTest = y(indices(splitBoundary + 1:length(y)), :);

% setting up the iterations
regParams = 1:2000;
iterations = length(regParams);
errorTraining = zeros(1, iterations);
errorTesting = zeros(1, iterations);

for i = 1:iterations
   [e1, model, e2] = polyReg(xTrain, yTrain, regParams(i), xTest, yTest);
   errorTraining(i) = e1;
   errorTesting(i) = e2;
end

% Choosing a value of lambda. Since the testing error is 
% constinously decreasing (asymptotic) the idea here is to 
% select a tolerance and choose the lambda where the 
% subsequent iterations give an error of less than tolerance.
delta = 1;  tolerance = 0.005; i = 2; minErrorLambda = 0;
while delta > tolerance && i <= 2000
    delta = abs(errorTesting(i) - errorTesting(i-1));
    i = i + 1;
    minErrorLambda = i-1;
end

clf;
plot(regParams, errorTraining, 'r', regParams, errorTesting, 'b');
hold on;
plot(minErrorLambda, errorTesting(minErrorLambda), 'kx');
t = strcat('\lambda at which \delta < tolerance:', ' ', num2str(minErrorLambda));
text(minErrorLambda, 2 + errorTesting(minErrorLambda), t, 'FontSize', 13);
title('Regularized MultiDimensional Regression');
xlabel('\lambda');
ylabel('Error');
legend('Training Error', 'Testing Error');

figure
plot(1./regParams, errorTraining, 'r', 1./regParams, errorTesting, 'b');
hold on;
plot(1./minErrorLambda, errorTesting(minErrorLambda), '*');
t = strcat('\lambda at which \delta < tolerance:', ' ', num2str(minErrorLambda));
text(1./minErrorLambda, 2 + errorTesting(minErrorLambda), t, 'FontSize', 13);
title('Regularized MultiDimensional Regression');
xlabel('1/\lambda');
ylabel('Error');
legend('Training Error', 'Testing Error');
