%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author: Prakhar Srivastav 
% UNI: PS2894
% Machine Learning - Tony Jebara.
% Date: 30 Sept 2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Regression function for HW2. Fits a multi-variate 
% regression function on the dataset
% @params
%   xTraining - x values of training data
%   yTraining - y values of training data
%   lambda - Regularization parameter
%   xTesting - x values of testing data
%   yTesting - y values of testing data
% @returns
%   errTraining - model error on training data
%   model - model parameters
%   errTesting - model error on testing data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [errTraining, model, errTesting] = polyReg(xTraining, yTraining, lambda, xTesting, yTesting)
    [rowCount, colCount] = size(xTraining);

    % prepare X for xTesting
    D = colCount + 1;
    xxTraining = ones(rowCount, D);
    for i=2:D
        xxTraining(:, i) = xTraining(:, i-1);
    end

    % build the model
    model = (xxTraining' * xxTraining + (lambda * eye(D)))\xxTraining'*yTraining;

    % find error in the training data
    errTraining = (1/(2 * rowCount)) * sum((yTraining - (xxTraining * model)).^2) + ...
                   (lambda * (norm(model))^2 / (2 * rowCount));

    % prepare X for xTesting
    [rowCountTesting, c] = size(xTesting);
    xxTesting = ones(rowCountTesting, D);
    for i=2:D
        xxTesting(:, i) = xTesting(:, i-1);
    end

    errTesting = (1/(2 * rowCountTesting)) * sum((yTesting - (xxTesting * model)).^2) + ...
                   (lambda * (norm(model))^2 / (2 * rowCount));
end
