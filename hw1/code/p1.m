%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author: Prakhar Srivastav 
% UNI: PS2894
% Machine Learning - Tony Jebara.
% Date: 30 Sept 2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Driver Program for HW1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Reading in the data
A = importdata('problem1.mat');
x = A.x; y = A.y; N = length(x); indices = randperm(N);

% 50-50 split between train and test data
splitBoundary = 0.5 * N; 

% create the training dataset
xTrain = x(indices(1:splitBoundary));
yTrain = y(indices(1:splitBoundary));

% create the test dataset
xTest = x(indices(splitBoundary + 1:length(x)));
yTest = y(indices(splitBoundary + 1:length(y)));

% for running multiple times
iterations = 50;
errTrainingSet = zeros(1, iterations);
errTestSet = zeros(1, iterations);
for d = 1:iterations
    [e1, m, e2] = multiReg(xTrain, yTrain, d, xTest, yTest, false);
    errTrainingSet(d) = e1;
    errTestSet(d) = e2;
end

% plot error training and error testing
clf
plot(1:iterations, errTrainingSet, 1:iterations, errTestSet);
title('Error vs degree of polynomial');
legend('training set error', 'test set error');

% find min error testing
[minError, index] = min(errTestSet);
t = strcat('\downarrow Min testing error: ', num2str(index));
text(index, 20*minError, t);

% Run one specific iteration
D = index;
figure
[errTrain, model, errTest] = multiReg(xTrain, yTrain, D, xTest, yTest, true);
