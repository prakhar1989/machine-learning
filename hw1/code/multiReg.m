%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author: Prakhar Srivastav 
% UNI: PS2894
% Machine Learning - Tony Jebara.
% Date: 30 Sept 2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Regression function for HW1. Fits a model of
% D dimensions on the training data.
% @params
%   xTraining - x values of training data
%   yTraining - y values of training data
%   D - Dimension
%   xTesting - x values of testing data
%   yTesting - y values of testing data
% @returns
%   errTraining - model error on training data
%   model - model parameters
%   errTesting - model error on testing data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [errTraining, model, errTesting] = multiReg(xTraining, yTraining, D, xTesting, yTesting, plotChart)
    N = length(xTraining);
    xx = zeros(length(xTraining), D);

    % greater degree polynomials at lower indices
    for i=1:D
        xx(:, i) = xTraining .^ (D-i);
    end

    % generate the model
    model = pinv(xx)*yTraining;

    % get the error
    errTraining = (1/(2 * N)) * sum((yTraining - (xx * model)).^2);

    % working the model on the testing data
    xxT = zeros(length(xTesting), D);
    for i = 1:D
        xxT(:, i) = xTesting .^ (D-i);
    end
    errTesting = (1/(2 * length(xTesting))) * sum((yTesting - (xxT * model)).^2);
    
    % prediction on testing data
    predictedY = xxT * model;
    
    % plotting
    if plotChart
        clf
        plot(xTesting, yTesting, 'x', xTesting, predictedY, '*');
        title(strcat('CrossValidation Polynomial Fitting for degree = ', num2str(D)));
        xlabel('X');
        ylabel('Y');
        legend('true values', 'predicted values');
    end
end

