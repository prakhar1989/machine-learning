%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author: Prakhar Srivastav
% UNI: PS2894
% Machine Learning - Tony Jebara.
% Date: 14 Nov 2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Runs Kmeans over the image
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% load the image
raw_im = Tiff('trees.tif', 'r');
im = raw_im.readRGBAImage();
im = im2double(im(1:200, 1:200, :));

%% initialze the program configurable parameters
k = 7; centroids = zeros(k, 3); maxCount = 5; N = length(im);
for i = 1:k
    centroids(i, :) = im(randi(N), randi(N), :);
end

%% running for a max of maxCount iterations
tic
for iter = 1:maxCount 
    
    %% calculate distances
    clusterInfo = zeros(N, N);
    for i = 1:N
        for j = 1:N
            distances = zeros(1, k);
            for l = 1:k
                point = squeeze(im(i, j, :));
                distances(l) = norm(point' - centroids(l, :));
            end
            [val, index] = min(distances);
            clusterInfo(i, j) = index;
        end
    end
    
    %% find new clusters
    for i = 1:k
        cluster = clusterInfo == i;
        t = repmat(cluster, 1, 1, 3) .* im;
        newC = squeeze(sum(sum(t))) / length(cluster(cluster==1));
        norm(newC' - centroids(i, :))
        centroids(i, :) = newC;
    end
    new_im = zeros(N, N, 3);
    for i = 1:N
        for j = 1:N
            new_im(i, j, :) = centroids(clusterInfo(i, j), :);
        end
    end
    
    %% save the image for each iteration
    imwrite(new_im, strcat('my_new_img-',int2str(iter),'.tif'), 'tif');
end
toc
