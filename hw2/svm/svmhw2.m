global p1; global p2;
load('hw2-testingdata.mat')
load('hw2-trainingdata.mat')

% %code for generating splits.. not required
% %for subsequent runs
% A = importdata('hw2-2015-dataset.mat');
% X = A.X; Y = A.Y; N = length(X); 
% 
% % replacing Y = 0 with Y = -1
% Y(Y==0) = -1;
% 
% % 50-50 split between train and test data
% indices = randperm(N); splitBoundary = 0.5 * N;
% 
% % create the training dataset
% xTrain = X(indices(1:splitBoundary), :);
% yTrain = Y(indices(1:splitBoundary), :);
% 
% % create the test dataset
% xTest = X(indices(splitBoundary + 1:length(X)), :);
% yTest = Y(indices(splitBoundary + 1:length(Y)), :);


% set ker to one of linear, poly or rbf.
ker = 'rbf';

% configuration for linear
if strcmp(ker,'linear') == 1
    cs = 1:5:100; p1 = 1; p2 = 0; sep = 1;
    errorsC = zeros(1, length(cs));
    for i = 1:length(cs)
        c = cs(i);
        errorsC(i) = getErrorCount(xTrain, yTrain, xTest, yTest, ker, c);
    end
    figure
    plot(cs, errorsC, '--*');
    title('SVM Classification with linear Kernel');
    xlabel('C');
    ylabel('Classification Error (%)');
    ylim([0 100]);
end

% configuration for poly
if strcmp(ker,'poly') == 1
    ps = 1:10; p2 = 0; sep = 1;
    errorsInf = zeros(1, length(ps));
    errorsC = zeros(1, length(ps));
    nsvInf = zeros(1, length(ps));
    nsvC = zeros(1, length(ps));
    for i = 1:length(ps)
        p1 = ps(i);
        [errorsInf(i), nsvInf(i)] = getErrorCount(xTrain, yTrain, xTest, yTest, ker, Inf);
        [errorsC(i), nsvC(i)] = getErrorCount(xTrain, yTrain, xTest, yTest, ker, 10);
    end
    figure
    plot(ps, errorsInf, '--+', ps, errorsC, ps, nsvInf, ':bs', ps, nsvC, ':rd');
    title('SVM Classification with Poly Kernel');
    legend('C = Inf', 'C = 100', '# Support Vectors for C = Inf', '# Support Vectors for C = 100');
    xlabel('Degree of the polynomial');
    ylabel('Classification Error (%)');
    ylim([0 100]);
end

% configuration for rbf
if strcmp(ker, 'rbf') == 1
    ps = 1:10;
    cs = 1:10:100;
    errors = zeros(length(ps), length(cs));
    nsvs = zeros(length(ps), length(cs));

    for i = 1:length(ps)
        p1 = ps(i);
        for j = 1:length(cs) 
            c = cs(j);
            [errors(i, j), nsvs(i, j)] = getErrorCount(xTrain, yTrain, xTest, yTest, ker, c);
        end
    end
    minError = min(min(errors));
    meshc(ps, cs, errors)
    zlabel('Classification error (%)');
    xlabel('\sigma');
    ylabel('C');
    title('SVM classification with RBF kernel');
end

